<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Registration Confirmation</title>
	</head>
	
	<body>
	
		<%
		 	String appType = session.getAttribute("appType").toString();
			if(appType.equals("friends")){
				appType = "Friends";
			}
			else if(appType.equals("smedia")){
				appType = "Social Media";
			}
			else{
				appType = "Others";
			}
		%>
		
		<h1>Registration confirmation</h1>
		<p>First Name: <%= session.getAttribute("fname") %></p>
		<p>Last Name: <%= session.getAttribute("lname") %></p>
		<p>Phone: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>App Discovery: <%= appType %></p>
		<p>Date of Birth: <%= session.getAttribute("dateOfBirth") %></p>
		<p>User Type: <%= session.getAttribute("usertype") %></p>
		<p>Description: <%= session.getAttribute("pdesc") %></p>
		
		<!-- Submit button for register -->
		<form action="login" method="post">
			<input type="submit">
		</form>
		
		<!-- Button to go back at index.jsp -->
		<form action="index.jsp">
			<input type="submit" value="Back">
		</form>
	
	</body>
</html>