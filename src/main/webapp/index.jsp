<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Job Finder Servlet</title>
		<style>
		div {
			margin-top: 5px;
			margin-bottom: 5px;
		}
		div > input, div > select, div > textarea{
			width: 25%;
		}
	</style>
	</head>
<body>

	<div id="container">
	<h1>Welcome to Servlet Job Finder!</h1>
		<form action="register" method="post">
			<!-- firstname -->
			<div>
			<label for="fname">First Name</label>
			<input type="text" name="fname" required>
			</div>
			
			<!-- lastname -->
			<div>
			<label for="lname">Last Name</label>
			<input type="text" name="lname" required>
			</div>
			
			<!-- Phone Number -->
			<div>
			<label for="phone">Phone</label>
			<input type="tel" name="phone" required>
			</div>
			
			<!-- Email -->
			<div>
			<label for="email">Email</label>
			<input type="email" name="email" required>
			</div>
			
			<!-- App Discovery -->
			<fieldset>
				<legend>How did you discover the app?</legend>
				<!-- Friends -->
				<input type="radio" id="friends" name="app_type" value="friends" required>
				<label for="friends">Friends</label><br>
				
				<!-- Social Media -->
				<input type="radio" id="s_media" name="app_type" value="smedia" required>
				<label for="s_media">Social Media</label><br>
				
				<!-- Others -->
				<input type="radio" id="others" name="app_type" value="others" required>
				<label for="others">Others</label>
			</fieldset>
			
			
			<!-- Date of Birth -->
			<div>
				<label for="date_of_birth">Date of Birth</label>
				<input type="date" name="date_of_birth" required>
			</div>
			
			<!-- usertype -->
			<div>
				<label for="user_type">Are you an Employer or Applicant?</label>
				<select id="u_type" name="user_type">
					<option value="" selected="selected"> Select One</option>
					<option value="applicant">Applicant</option>
					<option value="employer">Employer</option>
				</select>
			</div>
			
			
			<!-- Profile Description -->
			<div>
				<label for="pdesc">Profile Description</label>
				<textarea name="pdesc" maxlength="500"></textarea>
			</div>
			
			<!-- Button for Register -->
			<button>Register</button>
		</form>
	</div>

</body>
</html>