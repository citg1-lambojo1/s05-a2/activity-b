<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Home</title>
	</head>
	
	<body>
		<%
		 	String usertype = session.getAttribute("usertype").toString();
			if(usertype.equals("applicant")){
				usertype = "Applicant";
			}
			else{
				usertype = "Employer";
			}
		%>

		<h1>Welcome <%= session.getAttribute("fullname") %>!</h1>
		<%
				if(usertype == "Applicant"){
					out.println("Welcome applicant. You may now start looking for your career opportunity.");
				}
				else{
					out.println("Welcome employer. You may now start browsing applicant profiles.");
				}
		%>
	</body>
</html>